﻿using System;

namespace BlazorAppCore5.Models
{
    public class Tarefa
    {
        public int  Id { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime CompletedAt { get; set; }

    }
}
