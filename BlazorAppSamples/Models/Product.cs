﻿namespace BlazorAppSamples.Models
{
    public class Product
    {
        public string Title { get; set; }
        public DateTime CreateAt { get; set; } = DateTime.Now;
    }
}
